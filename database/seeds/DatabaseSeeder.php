<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        User::create([
            'username' => 'mas cipto',
            'email' => 'pratama.adiciptawan@gmail.com',
            'name' => 'Pratama',
            'role_id' => 1
        ]);

        Role::create([
            'name' => 'Pertama'
        ]);
    }
}
