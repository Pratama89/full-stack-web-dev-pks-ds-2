<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Data daftar role berhasil ditampilkan',
            'data'    => $roles
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allRequest = $request->all();
        //set validation
        $validator = Validator::make($allRequest, [
            'name'   => 'required'
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $role = Role::create([
            'name'     => $request->name
        ]);

        //success save to database
        if ($role) {

            return response()->json([
                'success' => true,
                'message' => 'Role berhasil di buat',
                'data'    => $role
            ], 200);
        }

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Data Role gagal dibuat',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);

        //make response JSON
        if ($role) {

            return response()->json([
                'success' => true,
                'message' => 'Detail Data Role',
                'data'    => $role
            ], 200);
        }

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Role Failed to Save',
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allRequest = $request->all();
        //set validation
        $validator = Validator::make($allRequest, [
            'name'   => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $role = Role::find($id);

        if ($role) {

            //update role
            $role->update([
                'name'     => $request->name,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Post ' . $role->name . ' Updated',
                'data'    => $role
            ], 200);
        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Post ' . $id . ' Not Found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id);

        if ($role) {

            //delete role
            $role->delete();

            return response()->json([
                'success' => true,
                'message' => 'Role Deleted',
            ], 200);
        }

        //data Role not found
        return response()->json([
            'success' => false,
            'message' => 'Role Not Found',
        ], 404);
    }
}
