<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $post_id = $request->post_id;

        //get data from table posts
        $comments = Comment::where('post_id', $post_id)->latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Data Comment berhasil di tampilkan',
            'data'    => $comments
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //find post by ID
        $comment = Comment::find($id);


        //make response JSON
        if ($comment) {

            return response()->json([
                'success' => true,
                'message' => 'Data Comment berhasil ditampilkan',
                'data'    => $comment
            ], 200);
        }

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Comment Failed with: ' . $id . ' not found',
        ], 404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //set validation
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'content'   => 'required',
            'post_id' => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = auth()->user();
        //save to database
        $comment = Comment::create([
            'content'     => $request->content,
            'post_id'   => $request->post_id,
            'user_id' => $user->id
        ]);

        //success save to database
        if ($comment) {

            return response()->json([
                'success' => true,
                'message' => 'Data Comment berhasil di buat',
                'data'    => $comment
            ], 200);
        }

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Comment Failed to Save',
        ], 409);
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'content'   => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $comment = Comment::find($id);

        if ($comment) {

            //update comment
            $comment->update([
                'content'     => $request->content,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Comment Updated',
                'data'    => $comment
            ], 200);
        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::find($id);

        if ($comment) {

            //delete comment
            $comment->delete();

            return response()->json([
                'success' => true,
                'message' => 'Comment Deleted',
            ], 200);
        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);
    }
}
