<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // dd('berhasil masuk');
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'otp'   => 'required'
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $otp_code = OtpCode::where('otp', $request->otp)->first();

        if (!$otp_code) {
            return response()->json([
                'success' => false,
                'message' => 'OTP Code tidak di temukan'
            ], 400);
        }

        $now = Carbon::now();

        if ($now > $otp_code->valid_until) {
            return response()->json([
                'success' => false,
                'message' => 'OTP Code tidak berlaku lagi'

            ], 400);
        }


        $user = User::find($otp_code->user_id);
        $user->update([
            'email_verified_at' => $now
        ]);

        $otp_code->delete();



        // $otp_code = OtpCode::create([
        //     'otp' => $random,
        //     'user_id' => $user->id,
        //     'valid_until' => $now->addMinutes(5)
        // ]);

        //kirim email otp code ke email register

        return response()->json([
            'success' => true,
            'message' => 'Otp Code berhasil diverifikasi',
            'data' => [
                'user' => $user,
                'otp_code' => $otp_code
            ]
        ], 200);
    }
}
